/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ['./src/**/*.{vue,md,js}'],
	theme: {
		extend: {
			colors:{
				primary: 'var(--vp-c-brand)'
			}
		},
	},
	plugins: [],
}
