# 安装

## 环境准备 {#prepare}

- Node js >= 14.16.0

推荐使用[nvm](https://github.com/nvm-sh/nvm)（默认是Linux版，[这里](https://github.com/coreybutler/nvm-windows)是Window版本）管理NodeJs的版本。

它为你一键安装、管理和切换多个版本的nodejs。

## 下载Egg {#download}

```bash
# gitee
git clone https://gitee.com/wallace5303/electron-egg.git

# github
git clone https://github.com/wallace5303/electron-egg.git

```

## 安装 {#install}

```bash
# 设置国内镜像源(加速)
npm config set registry=https://registry.npmmirror.com
npm config set disturl=https://registry.npmmirror.com/-/binary/node

#如果下载electron慢，配置
npm config set electron_mirror=https://registry.npmmirror.com/-/binary/electron/

# 进入目录 ./electron-egg/
npm install

# 如果还是提示 electron 没安装，进入 node_modules/electron 目录下，再npm install

# 构建sqlite
# - 需要 python3 环境 （操作系统自带）
# - 需要 node-gyp
npm i node-gyp -g
npm run re-sqlite
```

:::tip
[常见问题看这里](/docs/questions/normal)
:::