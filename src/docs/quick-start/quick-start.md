
# 快速开始

## 基础知识 {#base-info}

### 进程

框架有两种进程

- ee主进程：业务逻辑
- ee渲染进程：软件界面UI

### 通信

- ipc：前端 ⇋ 业务层 （双向通信）
- http： 前端、命令行、浏览器 ⇋ 业务层 （单向通信）
- socket：前端 ⇋ 业务层 （双向通信）

### 本地存储

- json数据库
- sqlite数据库

## 开发者模式 {#develop-mode}

**软件界面**

进入“frontend”目录，开发软件界面；支持任意前端技术，如果vue、react、angular、html等

```bash
# 进入【前端目录】
cd frontend 

# 安装依赖
npm install

# 启动服务
npm run serve
```

注：如果启动的前端服务不是 http:/<span>/localhost</span>:<span class="text-primary">8080</span> 请先配置：<span class="text-primary">config</span> 文件中的“开发模式”

**业务逻辑**

“electron”目录，业务开发；常规业务逻辑、调用操作系统api、访问远程服务器等
在项目【根目录】启动服务

```bash
# 启动后端服务
npm run dev

# 热重载模式
npm run reload
```

**demo**

下载的项目中，带有各种功能示例demo，初学者请先简单学习一下，方便快速入门。