# 介绍

## Egg 

<div class="flex flex-col items-center space-y-4">
	<img src="/assets/image/logo.png" />
	<div>一个入门简单、跨平台、桌面软件开发框架</div>
</div>

<div class="h-14"></div>

<div class="grid grid-cols-3">
	<div class="my-4 mx-2 border py-2 px-4 rounded-lg" v-for="it in [
		{name:'入门简单',desc:'只需懂 JavaScript'},
		{name:'跨平台',desc:'一套代码，打包成windows版、Mac版、Linux版'},
		{name:'开源免费',desc:'数万下载量，数百家中小企业在使用'},
		{name:'工程化',desc:'UI与业务分离，互相独立BSS开发思维'},
		{name:'高性能',desc:'事件驱动、非阻塞式IO'},
		{name:'功能demo',desc:'桌面软件常见功能，已提供demo，快速开发'},
	]">
		<div class="text-lg font-semibold">{{it.name}}</div>
		<div class="mt-1 text-gray-500 text-sm">{{it.desc}}</div>
	</div>
</div>

## 值得信赖 {#trust}

![图片](/assets/image/ee-zhengshu-1.png)

## 为什么使用 {#whyuse}

桌面软件（<span class="text-primary">办公方向、 个人工具</span>），仍然是<span class="text-primary">未来十几年PC端需</span>求之一，提高工作效率
<span class="text-primary">electron技术是流行趋势</span>，百度翻译、阿里网盘、迅雷、有道云笔记 ......
ee框架使用b（浏览器）s（主进程）s（远程后端服务）开发思想
<span class="text-primary">前端、服务端</span>同学都能快速入门

## 愿景 {#hope}

所有开发者都能学会桌面软件研发

## 简单 {#easy}

只需懂 JavaScript

## 开源 {#open}

<div class="flex items-center">
	<div>gitee：</div>
	<a href="https://gitee.com/wallace5303/electron-egg/stargazers" target="_blank">
		<img src="https://gitee.com/wallace5303/electron-egg/badge/star.svg?theme=white" alt="star" />
	</a>
</div>
<div class="mt-4 flex items-center">
	<div>github：</div>
	<a>[TODO]</a>
	<a href="https://github.com/wallace5303/electron-egg/stargazers" target="_blank">
		<img src="https://img.shields.io/badge/example-v1.0-red.svg" alt="star" />
	</a>
</div>


## 使用场景 {#usescene}

### 1. 常规桌面软件

windows平台