---
outline: deep
---

# 储存

本地存储服务，提供json数据库、sqlite数据库。
需要保存一些简单数据，在程序重启、操作系统重启后不丢失，可以选择Storage。
:::danger
前端模块直接调用 local-storage / session-storage 的方式不建议使用

因为在前端的storage是由electron管理的，目前存在一些不稳定的问题

实测windows下，运行过程中由于内存占用等原因，local-storage可能不定时被清空
:::

## 如何选择 {#how-choose}

### 常用场景 {#scene}

- 用户登陆时记住帐号、密码 
- 用户的本地配置

### 具体差异 {#diffrence}

|  [TODO]待确认  |     json      |     sqlit     |
| :------------: | :-----------: | :-----------: |
|    储存介质    |     硬盘      |     硬盘      |
|    储存位置    | 软件安装目录? | 软件安装目录? |
| 应用升级后保持 |      ×?       |      √?       |
|  支持复杂数据  |       ?       |       ?       |
|    最大容量    |       ?       |       ?       |

## 引入模块 {#import}

```js
const Storage = require('ee-core').Storage;
```

## json数据库 {#json}

### 连接 {#link}

```js
const jdb = Storage.JsonDB.connection('demo');
# 或
let lowdbOptions = {
  driver: 'lowdb'
}
const jdb = Storage.JsonDB.connection('demo', lowdbOptions);  
```

### 简单使用 {#jdb-easy-use}

```js

// 写入一个键值对
jdb.setItem('test_key', {name:'xiaoming'})

// 读取
jdb.getItem('test_key')

```

### 完全使用 {#jdb-full-use}

:::tip
lowdb文档：https://www.npmjs.com/package/lowdb
:::

```js

# 访问db
const jdb = Storage.JsonDB.connection('demo', {});
const db = jdb.db

# 添加对象和数据
db.defaults({posts: [], user: {}, count: 0})
  .write();
 
db.get('posts')
  .push({id: 1, title: 'lowdb is awesome'})
  .write()
 
db.set('user.name', 'typicode')
  .write()
 
db.update('count', n => n + 1)
  .write()

# 运行程序会在项目中添加db.json文件，里面存储了添加的数据：
{
  "posts": [
    {
      "id": 1,
      "title": "lowdb is awesome"
    }
  ],
  "user": {
    "name": "typicode"
  },
  "count": 1
}

# 可以使用lodash的各种函数，支持链式调用
db.get('users')
  .find({sex: 'male'})
  .value()

```


## sqlite数据库 {#sqlit}

### 连接 {#sqlit-connect}

```js
# sqlite数据库
let sqliteOptions = {
  driver: 'sqlite',
  default: {
  timeout: 6000,
    verbose: console.log // 打印sql语法
  }
}
const sdb = Storage.JsonDB.connection('sqlite-demo.db', sqliteOptions);
```

### sdb.db {#sqlit-db}

```js
# 插入数据    
const insert = sdb.db.prepare(`INSERT INTO ${table} (name, age) VALUES (@name, @age)`);
insert.run(data);
```


## ElectronStore模块 {#electron-store}

[electron store](https://github.com/sindresorhus/electron-store) 一个更简单的替代 local-storage 的方案

### 特性 {#estore-feature}

- 本地持久化储存

数据保存在名为 config.json 的 JSON 文件中，可通过app.getPath('userData')访问。

- 存储过程原子化

不会因存储过程中异常退出导致数据结构被破坏

- 支持数据结构迁移

[数据结构迁移](https://github.com/sindresorhus/electron-store#migrations)

:::warning
仅在渲染进程中使用时，需要在主进程中调用 Store.initRenderer() 或者 new Store() 来进行初始化
:::

### 安装 {#estore-install}

```bash
# 在electron层（根目录）安装
npm instal electron-store --save

```

### 简单使用 {#estore-use-simple}

```js
# /main.js
const ElectronStore = require('electron-store')
class Main extends Appliaction{
  /**
   * electron app ready
   */
  async electronAppReady() {
    // do some things
    // 初始化
    ElectronStore.initRenderer();

    // 实例化
    const store = new ElectronStore()

    // 赋值
    store.set('test-key', 123)

    // 取值
    console.log(store.get('test-key'))
  }
}

```

### 完全使用 {#estore-use-full}
[TODO] 实测发现在前端层js文件中也可以直接访问和读写electron-store，但会抛出一些文件引用失败，不确定复杂功能是否正常可用

注意要先在electron层窗体加载之前调用 const store = new ElectronStore(); 或者 ElectronStore.initRenderer();

```js
// 某个前端层文件
const ElectronStore = window.require('electron-store')

const store = new ElectronStore()

// 如果在electron层设置了该值，这里是可以正常读取的
console.log(store.get('test-key'))

// 这里写入值，在electron层定时打印，发现是可以修改成功的
store.set('test-key', 'frontend msg')

# 出现的问题是前端会抛出若干js文件引用失败的警告，不确定是否会影响其他复杂功能

```

