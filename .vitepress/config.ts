import { defineConfigWithTheme, HeadConfig, defineConfig } from 'vitepress'
import createSider from './main-sider'
// import type { Config as ThemeConfig } from '@vue/theme'
// import baseConfig from '@vue/theme/config'

const nav = [
	{ text: '导航栏', link: '' },
	{
		text: '多菜单', items: [
			{ text: 'Item A', link: '' },
			{ text: 'Item B', link: '' },
			{ text: 'Item C', link: '' }
		]
	}
]


const head = [
] as HeadConfig[]

/** 如果需要自定义主题可以使用 defineConfigWithTheme，它接收的themeConfig属性是可自定义的泛型 */
export default defineConfig({
	lang: 'zh-CN',
	lastUpdated: true,
	title: 'electron-egg.js',
	description: '一个入门简单、跨平台、企业级桌面软件开发框架',
	srcDir: 'src',
	head,
	themeConfig: {
		nav,
		sidebar: createSider(),
		algolia: {
			indexName: 'vuejs',
			appId: 'ML0LEBN7FQ',
			apiKey: 'f49cbd92a74532cc55cfbffa5e5a7d01',
			searchParameters: {
				facetFilters: ['version:v3']
			}
		},
		socialLinks: [
			{ icon: 'github', link: 'https://github.com/vuejs/' },
			{ icon: 'twitter', link: 'https://twitter.com/vuejs' },
			{ icon: 'discord', link: 'https://discord.com/invite/HBherRA' }
		],
	}
})