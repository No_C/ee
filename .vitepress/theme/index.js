import DefaultTheme from 'vitepress/theme'
import './tailwind.css'

/**
 * 这相当于是一个全局引入的文件
 */

export default {
	...DefaultTheme,
}