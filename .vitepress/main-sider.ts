import { DefaultTheme } from 'vitepress'

const createSider = (): DefaultTheme.SidebarGroup[] => [
	{
		text: '开始',
		collapsible: true,
		items: [
			{ text: '简介', link: '/docs/quick-start/introduce' },
			{ text: '安装', link: '/docs/quick-start/install' },
			{ text: '快速开始', link: '/docs/quick-start/quick-start' },
		]
	},
	{
		text: '基础',
		collapsible: true,
		items: [
			{ text: '目录结构', link: '/todo' },
			{ text: '配置', link: '/todo' },
			{ text: '入口及生命周期', link: '/todo' },
			{ text: '前端模块', link: '/todo' },
			{ text: '控制器（controller）', link: '/todo' },
			{ text: '服务层（service）', link: '/todo' },
			{ text: '预加载层（preload）', link: '/todo' },
			{ text: '通信（ipcRender）', link: '/todo' },
			{ text: '数据库', link: '/todo' },
			{ text: '日志', link: '/todo' },
			{ text: '调试', link: '/todo' },
			{ text: '额外资源目录', link: '/todo' },
			{ text: '脚本工具', link: '/todo' },
			{ text: '远程地址', link: '/todo' },
		]
	},
	{
		text: '生产构建',
		collapsible: true,
		items: [
			{ text: '打包', link: '/todo' },
			{ text: '图标修改', link: '/todo' },
			{ text: '桌面图标工具', link: '/todo' },
		]
	},
	{
		text: '升级',
		collapsible: true,
		items: [
			{ text: '框架升级', link: '/todo' },
			{ text: 'electron与node.js版本关系', link: '/todo' },
			{ text: '应用软件自动升级', link: '/todo' },
		]
	},
	{
		text: 'API/功能',
		collapsible: true,
		items: [
			{ text: 'app全局对象', link: '/todo' },
			{ text: 'Storage', link: '/docs/api/storage' },
			{ text: 'Utils', link: '/todo' },
			{ text: 'Socket', link: '/todo' },
			{ text: 'DLL使用', link: '/todo' },
			{ text: '调用第三方程序', link: '/todo' },
		]
	},
	{
		text: '常见问题',
		collapsible: true,
		items: [],
	},
	{
		text: '项目案例',
		collapsible: true,
		items: [],
	},
	{
		text: '交流',
		collapsible: true,
		items: [],
	}
]

export default createSider 