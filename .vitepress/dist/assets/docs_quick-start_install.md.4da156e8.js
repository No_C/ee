import{_ as s,o as n,c as a,a as e}from"./app.698d8cbc.js";const m=JSON.parse('{"title":"\u5B89\u88C5","description":"","frontmatter":{},"headers":[{"level":2,"title":"\u73AF\u5883\u51C6\u5907","slug":"prepare"},{"level":2,"title":"\u4E0B\u8F7DEgg","slug":"download"},{"level":2,"title":"\u5B89\u88C5","slug":"install"}],"relativePath":"docs/quick-start/install.md","lastUpdated":1669365954000}'),l={name:"docs/quick-start/install.md"},t=e(`<h1 id="\u5B89\u88C5" tabindex="-1">\u5B89\u88C5 <a class="header-anchor" href="#\u5B89\u88C5" aria-hidden="true">#</a></h1><h2 id="prepare" tabindex="-1">\u73AF\u5883\u51C6\u5907 <a class="header-anchor" href="#prepare" aria-hidden="true">#</a></h2><ul><li>Node js &gt;= 14.16.0</li></ul><p>\u63A8\u8350\u4F7F\u7528<a href="https://github.com/nvm-sh/nvm" target="_blank" rel="noopener noreferrer">nvm</a>\uFF08\u9ED8\u8BA4\u662FLinux\u7248\uFF0C<a href="https://github.com/coreybutler/nvm-windows" target="_blank" rel="noopener noreferrer">\u8FD9\u91CC</a>\u662FWindow\u7248\u672C\uFF09\u7BA1\u7406NodeJs\u7684\u7248\u672C\u3002</p><p>\u5B83\u4E3A\u4F60\u4E00\u952E\u5B89\u88C5\u3001\u7BA1\u7406\u548C\u5207\u6362\u591A\u4E2A\u7248\u672C\u7684nodejs\u3002</p><h2 id="download" tabindex="-1">\u4E0B\u8F7DEgg <a class="header-anchor" href="#download" aria-hidden="true">#</a></h2><div class="language-bash"><span class="copy"></span><pre><code><span class="line"><span style="color:#676E95;font-style:italic;"># gitee</span></span>
<span class="line"><span style="color:#A6ACCD;">git clone https://gitee.com/wallace5303/electron-egg.git</span></span>
<span class="line"></span>
<span class="line"><span style="color:#676E95;font-style:italic;"># github</span></span>
<span class="line"><span style="color:#A6ACCD;">git clone https://github.com/wallace5303/electron-egg.git</span></span>
<span class="line"></span>
<span class="line"></span></code></pre></div><h2 id="install" tabindex="-1">\u5B89\u88C5 <a class="header-anchor" href="#install" aria-hidden="true">#</a></h2><div class="language-bash"><span class="copy"></span><pre><code><span class="line"><span style="color:#676E95;font-style:italic;"># \u8BBE\u7F6E\u56FD\u5185\u955C\u50CF\u6E90(\u52A0\u901F)</span></span>
<span class="line"><span style="color:#A6ACCD;">npm config </span><span style="color:#82AAFF;">set</span><span style="color:#A6ACCD;"> registry=https://registry.npmmirror.com</span></span>
<span class="line"><span style="color:#A6ACCD;">npm config </span><span style="color:#82AAFF;">set</span><span style="color:#A6ACCD;"> disturl=https://registry.npmmirror.com/-/binary/node</span></span>
<span class="line"></span>
<span class="line"><span style="color:#676E95;font-style:italic;">#\u5982\u679C\u4E0B\u8F7Delectron\u6162\uFF0C\u914D\u7F6E</span></span>
<span class="line"><span style="color:#A6ACCD;">npm config </span><span style="color:#82AAFF;">set</span><span style="color:#A6ACCD;"> electron_mirror=https://registry.npmmirror.com/-/binary/electron/</span></span>
<span class="line"></span>
<span class="line"><span style="color:#676E95;font-style:italic;"># \u8FDB\u5165\u76EE\u5F55 ./electron-egg/</span></span>
<span class="line"><span style="color:#A6ACCD;">npm install</span></span>
<span class="line"></span>
<span class="line"><span style="color:#676E95;font-style:italic;"># \u5982\u679C\u8FD8\u662F\u63D0\u793A electron \u6CA1\u5B89\u88C5\uFF0C\u8FDB\u5165 node_modules/electron \u76EE\u5F55\u4E0B\uFF0C\u518Dnpm install</span></span>
<span class="line"></span>
<span class="line"><span style="color:#676E95;font-style:italic;"># \u6784\u5EFAsqlite</span></span>
<span class="line"><span style="color:#676E95;font-style:italic;"># - \u9700\u8981 python3 \u73AF\u5883 \uFF08\u64CD\u4F5C\u7CFB\u7EDF\u81EA\u5E26\uFF09</span></span>
<span class="line"><span style="color:#676E95;font-style:italic;"># - \u9700\u8981 node-gyp</span></span>
<span class="line"><span style="color:#A6ACCD;">npm i node-gyp -g</span></span>
<span class="line"><span style="color:#A6ACCD;">npm run re-sqlite</span></span>
<span class="line"></span></code></pre></div><div class="tip custom-block"><p class="custom-block-title">TIP</p><p><a href="/docs/questions/normal.html">\u5E38\u89C1\u95EE\u9898\u770B\u8FD9\u91CC</a></p></div>`,10),p=[t];function o(r,c,i,d,h,y){return n(),a("div",null,p)}var A=s(l,[["render",o]]);export{m as __pageData,A as default};
